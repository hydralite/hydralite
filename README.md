# Welcome to Hydralite
<p align="center">
  <img width="200px" src="https://raw.githubusercontent.com/hydralite/hydralite/dev/assets/logos/Hydralite%20Logo(TransBG).png" />
</p>

## What Is Hydralite?

Hydralite is the next-generation project marketing and management platform that uses a platform-specific "currency" called Hydra, which enables you to build a vast community around your product using an algorithmic, promotional feed of content, as well as to conduct your software effectively with an intuitive, one-of-a-kind product management framework.

## Sponsors
<a href="https://yomo.run">
   <img src="https://github.com/hydralite/hydralite/blob/dev/assets/sponsors/yomo.png?raw=true" />
</a>
